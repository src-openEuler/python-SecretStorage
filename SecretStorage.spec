%global _empty_manifest_terminate_build 0
%global package_description \
	This module provides a way for securely storing passwords and other secrets.
Name:           python-SecretStorage
Version:        3.3.3
Release:        1
Summary:        Python bindings to FreeDesktop.org Secret Service API
License:        BSD-3-Clause
URL:            https://github.com/mitya57/secretstorage
Source0:        %{pypi_source SecretStorage}
BuildArch:      noarch
%description
%{package_description}

%package -n python3-SecretStorage
Summary:        Python bindings to FreeDesktop.org Secret Service API
Provides:       python-secretstorage
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-cryptography
BuildRequires:  python3-jeepney
# General requires
Requires:       python3-cryptography
Requires:       python3-jeepney
%description -n python3-SecretStorage
%{package_description}

%package help
Summary:        Python bindings to FreeDesktop.org Secret Service API
Provides:       python3-secretstorage-doc
%description help
%{package_description}

%prep
%autosetup -n SecretStorage-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%check
# %{__python3} setup.py test

%files -n python3-SecretStorage -f filelist.lst
%dir %{python3_sitelib}/*

%files help
%{_docdir}/*

%changelog
* Wed Jun 07 2023 chenchen <chen_aka_jan@163.com> - 3.3.3-1
- Update to 3.3.3

* Fri Jul 08 2022 chenchen <chen_aka_jan@163.com> - 3.3.2-1
- Update to 3.3.2

* Wed May 11 2022 wulei <wulei80@h-partners.com> - 3.3.1-2
- License compliance rectification

* Mon Aug 09 2021 OpenStack_SIG <openstack@openeuler.org> - 3.3.1-1
- Package update to 3.3.1

* Tue Dec 1 2020 baizhonggui <baizhonggui@huawei.com> - 3.3.0-1
- Update to 3.3.0

* Mon Aug 10 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 2.3.1-12
- Remove python2

* Mon Nov 25 2019 lihao <lihao129@huawei.com> - 2.3.1-11
- Package Init

* Fri Nov 22 2019 lihao <lihao129@huawei.com> - 2.3.1-10.h1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix testcase error
